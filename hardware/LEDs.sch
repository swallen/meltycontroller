EESchema Schematic File Version 4
LIBS:meltyControllerGen2_2-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Transistor_BJT:MMBT3904 Q2
U 1 1 5D95C67B
P 3500 5050
F 0 "Q2" H 3691 5096 50  0000 L CNN
F 1 "MMBT3904" H 3691 5005 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 3700 4975 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 3500 5050 50  0001 L CNN
	1    3500 5050
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:MMBT3904 Q3
U 1 1 5D95C835
P 3500 5800
F 0 "Q3" H 3691 5846 50  0000 L CNN
F 1 "MMBT3904" H 3691 5755 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 3700 5725 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 3500 5800 50  0001 L CNN
	1    3500 5800
	1    0    0    -1  
$EndComp
$Comp
L Transistor_BJT:MMBT3904 Q4
U 1 1 5D95C8BB
P 3500 6550
F 0 "Q4" H 3691 6596 50  0000 L CNN
F 1 "MMBT3904" H 3691 6505 50  0000 L CNN
F 2 "TO_SOT_Packages_SMD:SOT-23" H 3700 6475 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N3904.pdf" H 3500 6550 50  0001 L CNN
	1    3500 6550
	1    0    0    -1  
$EndComp
$Comp
L Passives:Resistor R?
U 1 1 5D966308
P 3100 5050
AR Path="/5D966308" Ref="R?"  Part="1" 
AR Path="/5CC7450C/5D966308" Ref="R?"  Part="1" 
AR Path="/5D04372C/5D966308" Ref="R?"  Part="1" 
AR Path="/5D0435EE/5D966308" Ref="R17"  Part="1" 
F 0 "R17" H 3100 5245 50  0000 C CNN
F 1 "1k" H 3100 5154 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" H 3100 4980 60  0001 C CNN
F 3 "Passives.lib" H 3100 4980 60  0001 C CNN
	1    3100 5050
	1    0    0    -1  
$EndComp
$Comp
L Passives:Resistor R?
U 1 1 5D968794
P 3100 5800
AR Path="/5D968794" Ref="R?"  Part="1" 
AR Path="/5CC7450C/5D968794" Ref="R?"  Part="1" 
AR Path="/5D04372C/5D968794" Ref="R?"  Part="1" 
AR Path="/5D0435EE/5D968794" Ref="R18"  Part="1" 
F 0 "R18" H 3100 5995 50  0000 C CNN
F 1 "1k" H 3100 5904 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" H 3100 5730 60  0001 C CNN
F 3 "Passives.lib" H 3100 5730 60  0001 C CNN
	1    3100 5800
	-1   0    0    -1  
$EndComp
$Comp
L Passives:Resistor R?
U 1 1 5D9687EA
P 3100 6550
AR Path="/5D9687EA" Ref="R?"  Part="1" 
AR Path="/5CC7450C/5D9687EA" Ref="R?"  Part="1" 
AR Path="/5D04372C/5D9687EA" Ref="R?"  Part="1" 
AR Path="/5D0435EE/5D9687EA" Ref="R19"  Part="1" 
F 0 "R19" H 3100 6745 50  0000 C CNN
F 1 "1k" H 3100 6654 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" H 3100 6480 60  0001 C CNN
F 3 "Passives.lib" H 3100 6480 60  0001 C CNN
	1    3100 6550
	-1   0    0    -1  
$EndComp
$Comp
L Passives:Resistor R?
U 1 1 5D9688AE
P 3950 4650
AR Path="/5D9688AE" Ref="R?"  Part="1" 
AR Path="/5CC7450C/5D9688AE" Ref="R?"  Part="1" 
AR Path="/5D04372C/5D9688AE" Ref="R?"  Part="1" 
AR Path="/5D0435EE/5D9688AE" Ref="R20"  Part="1" 
F 0 "R20" H 3950 4845 50  0000 C CNN
F 1 "36.5" H 3950 4754 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" H 3950 4580 60  0001 C CNN
F 3 "Passives.lib" H 3950 4580 60  0001 C CNN
	1    3950 4650
	0    1    -1   0   
$EndComp
$Comp
L Passives:Resistor R?
U 1 1 5D96C3E2
P 4250 4650
AR Path="/5D96C3E2" Ref="R?"  Part="1" 
AR Path="/5CC7450C/5D96C3E2" Ref="R?"  Part="1" 
AR Path="/5D04372C/5D96C3E2" Ref="R?"  Part="1" 
AR Path="/5D0435EE/5D96C3E2" Ref="R21"  Part="1" 
F 0 "R21" H 4250 4845 50  0000 C CNN
F 1 "0" H 4250 4754 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" H 4250 4580 60  0001 C CNN
F 3 "Passives.lib" H 4250 4580 60  0001 C CNN
	1    4250 4650
	0    1    -1   0   
$EndComp
$Comp
L Passives:Resistor R?
U 1 1 5D96C42A
P 4550 4650
AR Path="/5D96C42A" Ref="R?"  Part="1" 
AR Path="/5CC7450C/5D96C42A" Ref="R?"  Part="1" 
AR Path="/5D04372C/5D96C42A" Ref="R?"  Part="1" 
AR Path="/5D0435EE/5D96C42A" Ref="R22"  Part="1" 
F 0 "R22" H 4550 4845 50  0000 C CNN
F 1 "0" H 4550 4754 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" H 4550 4580 60  0001 C CNN
F 3 "Passives.lib" H 4550 4580 60  0001 C CNN
	1    4550 4650
	0    1    -1   0   
$EndComp
$Comp
L Passives:Resistor R?
U 1 1 5D96C476
P 5500 4650
AR Path="/5D96C476" Ref="R?"  Part="1" 
AR Path="/5CC7450C/5D96C476" Ref="R?"  Part="1" 
AR Path="/5D04372C/5D96C476" Ref="R?"  Part="1" 
AR Path="/5D0435EE/5D96C476" Ref="R23"  Part="1" 
F 0 "R23" H 5500 4845 50  0000 C CNN
F 1 "36.5" H 5500 4754 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" H 5500 4580 60  0001 C CNN
F 3 "Passives.lib" H 5500 4580 60  0001 C CNN
	1    5500 4650
	0    1    -1   0   
$EndComp
$Comp
L Passives:Resistor R?
U 1 1 5D96C4C4
P 5800 4650
AR Path="/5D96C4C4" Ref="R?"  Part="1" 
AR Path="/5CC7450C/5D96C4C4" Ref="R?"  Part="1" 
AR Path="/5D04372C/5D96C4C4" Ref="R?"  Part="1" 
AR Path="/5D0435EE/5D96C4C4" Ref="R24"  Part="1" 
F 0 "R24" H 5800 4845 50  0000 C CNN
F 1 "0" H 5800 4754 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" H 5800 4580 60  0001 C CNN
F 3 "Passives.lib" H 5800 4580 60  0001 C CNN
	1    5800 4650
	0    1    -1   0   
$EndComp
$Comp
L Passives:Resistor R?
U 1 1 5D96C516
P 6100 4650
AR Path="/5D96C516" Ref="R?"  Part="1" 
AR Path="/5CC7450C/5D96C516" Ref="R?"  Part="1" 
AR Path="/5D04372C/5D96C516" Ref="R?"  Part="1" 
AR Path="/5D0435EE/5D96C516" Ref="R25"  Part="1" 
F 0 "R25" H 6100 4845 50  0000 C CNN
F 1 "0" H 6100 4754 50  0000 C CNN
F 2 "Resistors_SMD:R_0402" H 6100 4580 60  0001 C CNN
F 3 "Passives.lib" H 6100 4580 60  0001 C CNN
	1    6100 4650
	0    1    -1   0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D96F863
P 3600 5250
AR Path="/5D96F863" Ref="#PWR?"  Part="1" 
AR Path="/5CD8D347/5D96F863" Ref="#PWR?"  Part="1" 
AR Path="/5D0435EE/5D96F863" Ref="#PWR03"  Part="1" 
F 0 "#PWR03" H 3600 5000 50  0001 C CNN
F 1 "GND" V 3605 5122 50  0000 R CNN
F 2 "" H 3600 5250 50  0001 C CNN
F 3 "" H 3600 5250 50  0001 C CNN
	1    3600 5250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D96FB70
P 3600 6000
AR Path="/5D96FB70" Ref="#PWR?"  Part="1" 
AR Path="/5CD8D347/5D96FB70" Ref="#PWR?"  Part="1" 
AR Path="/5D0435EE/5D96FB70" Ref="#PWR04"  Part="1" 
F 0 "#PWR04" H 3600 5750 50  0001 C CNN
F 1 "GND" V 3605 5872 50  0000 R CNN
F 2 "" H 3600 6000 50  0001 C CNN
F 3 "" H 3600 6000 50  0001 C CNN
	1    3600 6000
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5D97023A
P 5500 3900
AR Path="/5D97023A" Ref="#PWR?"  Part="1" 
AR Path="/5CD8D347/5D97023A" Ref="#PWR?"  Part="1" 
AR Path="/5D0435EE/5D97023A" Ref="#PWR07"  Part="1" 
F 0 "#PWR07" H 5500 3750 50  0001 C CNN
F 1 "+3.3V" H 5515 4073 50  0000 C CNN
F 2 "" H 5500 3900 50  0001 C CNN
F 3 "" H 5500 3900 50  0001 C CNN
	1    5500 3900
	-1   0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5D97027F
P 3950 3900
AR Path="/5D97027F" Ref="#PWR?"  Part="1" 
AR Path="/5CD8D347/5D97027F" Ref="#PWR?"  Part="1" 
AR Path="/5D0435EE/5D97027F" Ref="#PWR08"  Part="1" 
F 0 "#PWR08" H 3950 3750 50  0001 C CNN
F 1 "+3.3V" H 3965 4073 50  0000 C CNN
F 2 "" H 3950 3900 50  0001 C CNN
F 3 "" H 3950 3900 50  0001 C CNN
	1    3950 3900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3600 4850 3600 4800
Wire Wire Line
	3600 4800 3950 4800
Wire Wire Line
	3950 4800 3950 4750
Wire Wire Line
	5500 4800 5500 4750
Wire Wire Line
	4250 4750 4250 5550
Wire Wire Line
	4250 5550 3600 5550
Wire Wire Line
	3600 5550 3600 5600
Wire Wire Line
	4550 4750 4550 6300
Wire Wire Line
	4550 6300 3600 6300
Wire Wire Line
	3600 6300 3600 6350
Wire Wire Line
	3300 5050 3200 5050
Wire Wire Line
	3300 5800 3200 5800
Wire Wire Line
	3300 6550 3200 6550
$Comp
L power:GND #PWR?
U 1 1 5D98A441
P 3600 6750
AR Path="/5D98A441" Ref="#PWR?"  Part="1" 
AR Path="/5CD8D347/5D98A441" Ref="#PWR?"  Part="1" 
AR Path="/5D0435EE/5D98A441" Ref="#PWR05"  Part="1" 
F 0 "#PWR05" H 3600 6500 50  0001 C CNN
F 1 "GND" V 3605 6622 50  0000 R CNN
F 2 "" H 3600 6750 50  0001 C CNN
F 3 "" H 3600 6750 50  0001 C CNN
	1    3600 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 4750 5800 5550
Wire Wire Line
	6100 4750 6100 6300
Text HLabel 2900 5050 0    50   Input ~ 0
R
Text HLabel 2900 5800 0    50   Input ~ 0
G
Text HLabel 2900 6550 0    50   Input ~ 0
B
Wire Wire Line
	3000 5050 2900 5050
Wire Wire Line
	3000 5800 2900 5800
Wire Wire Line
	3000 6550 2900 6550
$Comp
L device:LED D1
U 1 1 5DE1DC42
P 3950 4150
F 0 "D1" V 3988 4033 50  0000 R CNN
F 1 "LED" V 3897 4033 50  0000 R CNN
F 2 "U_LEDs:LED side mount 2-SMD 2.1x0.8" H 3950 4150 50  0001 C CNN
F 3 "~" H 3950 4150 50  0001 C CNN
	1    3950 4150
	0    -1   -1   0   
$EndComp
$Comp
L device:LED D3
U 1 1 5DE1DFF2
P 4250 4150
F 0 "D3" V 4288 4033 50  0000 R CNN
F 1 "LED" V 4197 4033 50  0000 R CNN
F 2 "U_LEDs:LED side mount 2-SMD 2.1x0.8" H 4250 4150 50  0001 C CNN
F 3 "~" H 4250 4150 50  0001 C CNN
	1    4250 4150
	0    -1   -1   0   
$EndComp
$Comp
L device:LED D6
U 1 1 5DE1E05A
P 4550 4150
F 0 "D6" V 4588 4033 50  0000 R CNN
F 1 "LED" V 4497 4033 50  0000 R CNN
F 2 "U_LEDs:LED side mount 2-SMD 2.1x0.8" H 4550 4150 50  0001 C CNN
F 3 "~" H 4550 4150 50  0001 C CNN
	1    4550 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3950 3900 3950 3950
Wire Wire Line
	3950 3950 4250 3950
Wire Wire Line
	4550 3950 4550 4000
Connection ~ 3950 3950
Wire Wire Line
	3950 3950 3950 4000
Wire Wire Line
	4250 4000 4250 3950
Connection ~ 4250 3950
Wire Wire Line
	4250 3950 4550 3950
$Comp
L device:LED D8
U 1 1 5DE4FAEC
P 5500 4150
F 0 "D8" V 5538 4033 50  0000 R CNN
F 1 "LED" V 5447 4033 50  0000 R CNN
F 2 "U_LEDs:LED side mount 2-SMD 2.1x0.8" H 5500 4150 50  0001 C CNN
F 3 "~" H 5500 4150 50  0001 C CNN
	1    5500 4150
	0    -1   -1   0   
$EndComp
$Comp
L device:LED D9
U 1 1 5DE500D3
P 5800 4150
F 0 "D9" V 5838 4033 50  0000 R CNN
F 1 "LED" V 5747 4033 50  0000 R CNN
F 2 "U_LEDs:LED side mount 2-SMD 2.1x0.8" H 5800 4150 50  0001 C CNN
F 3 "~" H 5800 4150 50  0001 C CNN
	1    5800 4150
	0    -1   -1   0   
$EndComp
$Comp
L device:LED D10
U 1 1 5DE504D7
P 6100 4150
F 0 "D10" V 6138 4033 50  0000 R CNN
F 1 "LED" V 6047 4033 50  0000 R CNN
F 2 "U_LEDs:LED side mount 2-SMD 2.1x0.8" H 6100 4150 50  0001 C CNN
F 3 "~" H 6100 4150 50  0001 C CNN
	1    6100 4150
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5500 3900 5500 3950
Wire Wire Line
	5500 3950 5800 3950
Wire Wire Line
	6100 3950 6100 4000
Wire Wire Line
	5800 4000 5800 3950
Connection ~ 5800 3950
Wire Wire Line
	5800 3950 6100 3950
Wire Wire Line
	5500 4000 5500 3950
Connection ~ 5500 3950
Wire Wire Line
	3950 4800 5500 4800
Connection ~ 3950 4800
Wire Wire Line
	4250 5550 5800 5550
Connection ~ 4250 5550
Wire Wire Line
	4550 6300 6100 6300
Connection ~ 4550 6300
Wire Wire Line
	6100 4300 6100 4550
Wire Wire Line
	5800 4300 5800 4550
Wire Wire Line
	5500 4300 5500 4550
Wire Wire Line
	4550 4300 4550 4550
Wire Wire Line
	4250 4300 4250 4550
Wire Wire Line
	3950 4300 3950 4550
$EndSCHEMATC
