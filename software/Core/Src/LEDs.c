/*
 * LEDs.c
 *
 *  Created on: Feb 23, 2019
 *      Author: swallen
 */

#include "LEDs.h"

TIM_TypeDef * indTimInst = TIM3;
SPI_TypeDef * onboardSPIInst = SPI1;
SPI_TypeDef * ext0SPIInst = SPI3;
SPI_TypeDef * ext1SPIInst = SPI2;

uint8_t ledTransmitBuffer[SPI_FRAME_SIZE];

/* HALO:
 * PA7	SPI1_MOSI	LED_onboard_DI
 * PA5	SPI1_SCK	LED_onboard_CLK
 *
 * PC10	SPI3_SCK	LED_ext0_clk
 * PC12	SPI3_MOSI	LED_ext0_DI
 *
 * PB13	SPI2_SCK	LED_ext1_clk
 * PB15	SPI2_MOSI	LED_ext1_DI
 *
 * PC6	TIM3_CH1	LED_ind_R
 * PC7	TIM3_CH2	LED_ind_G
 * PC8	TIM3_CH3	LED_ind_B
 */

/* HITNSPIN:
 * PC6	TIM3_CH1	LED_ind_R
 * PC7	TIM3_CH2	LED_ind_G
 * PB0	TIM3_CH3	LED_ind_B
 */

void LED_init() {
	initGPIO(GPIOC, 6, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_HIGH, GPIO_PULL_NONE, 2);
	initGPIO(GPIOC, 7, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_HIGH, GPIO_PULL_NONE, 2);
#ifdef HALO
	initGPIO(GPIOC, 8, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_HIGH, GPIO_PULL_NONE, 2);
#endif
#ifdef HITNSPIN
	initGPIO(GPIOB, 0, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_HIGH, GPIO_PULL_NONE, 2);
#endif

	//basic timer setup
	indTimInst->CR1 = 0;//make sure the timer is disabled
	indTimInst->PSC = (MAIN_CLOCK*2/4)/10-1;//cnt = (freq_in/freq_out)-1 -> cnt = (APB1clk=main_clk/4)*2/(10MHz) - 1;
	indTimInst->ARR = 255;//caps resolution to one byte. makes fout=39.22kHz
	indTimInst->CNT = 0;//reset the counter
	//PWM setup
	indTimInst->CCMR1 = 0x6868;//enables PWM mode 1 for channels 1 & 2
	indTimInst->CCMR2 = 0x0068;//enables PWM mode 1 for channel 3
	indTimInst->CR1 = 0x0080;//enable ARR preload
	//set the channel PWM % to 0
	indTimInst->CCR1 = 0;
	indTimInst->CCR2 = 0;
	indTimInst->CCR3 = 0;
	//enable OC outputs 1, 2, and 3
	indTimInst->CCER = 0x0111;

	indTimInst->EGR = 0x0001;//manually set the UG bit so that settings get loaded into shadow registers
	indTimInst->CR1 |= 0x0001;//enable the timer

#ifdef HALO
	//init the SPI outputs for the APA102 strips
	//onboard
	initGPIO(GPIOA, 5, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_HIGH, GPIO_PULL_NONE, 5);
	initGPIO(GPIOA, 7, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_HIGH, GPIO_PULL_NONE, 5);
	//ext0
	initGPIO(GPIOC, 10, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_HIGH, GPIO_PULL_NONE, 6);
	initGPIO(GPIOC, 12, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_HIGH, GPIO_PULL_NONE, 6);
	//ext1
	initGPIO(GPIOB, 13, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_HIGH, GPIO_PULL_NONE, 5);
	initGPIO(GPIOB, 15, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_HIGH, GPIO_PULL_NONE, 5);

	//prevent the SPI peripheral from erroring out due to SS pin not being connected
	onboardSPIInst->CR2 |= 0x0004;
	ext0SPIInst->CR2 |= 0x0004;
	ext1SPIInst->CR2 |= 0x0004;

	//enable the SPI peripheral and set to master mode
	onboardSPIInst->CR1 = 0x0044;
	ext0SPIInst->CR1 = 0x0044;
	ext1SPIInst->CR1 = 0x0044;


	//set up the transmit buffer
	for(int i=0;i<SPI_FRAME_SIZE-1;i++) ledTransmitBuffer[i] = 0;//first four bytes are start frame, clear the rest of the frames too
	for(int i=0;i<NUM_SERIAL_LEDS;i++) ledTransmitBuffer[i*4+4] = 0xFF;//set the three required bits for each led frame, plus the global brightness
	for(int i=7*4+4;i<SPI_FRAME_SIZE;i++) ledTransmitBuffer[i] = 0xFF;//set the end frames
#endif
}

void sendToSPILEDs(SPI_TypeDef * inst, uint8_t * buf, uint8_t size) {
	for(uint8_t i=0; i<size; i++) {

		//wait until the buffer empties
		while((inst->SR & 0x0002) == 0);

		inst->DR = (uint16_t) (*buf++);//shift the next byte in
	}
}

void setIndicatorLEDs(uint16_t red, uint16_t green, uint16_t blue) {
#ifdef HALO
	//Spencer done messed one up good
	//Red and green LEDs populated backwards.
	indTimInst->CCR1 = green;
	indTimInst->CCR2 = red;
	indTimInst->CCR3 = blue;
#endif
#ifdef HITNSPIN
	indTimInst->CCR1 = red;
	indTimInst->CCR2 = green;
	indTimInst->CCR3 = blue;
#endif
}

void updateLEDs(Frame_P f) {
	//map the frame data into the buffer
	for(uint8_t i=0;i<NUM_SERIAL_LEDS;i++) {
		//i+0 is the global brightness value, which should never be less than max or bad flickering
		uint8_t bufLoc = APA102_HEADER_SIZE + i*APA102_DATA_SIZE;
		ledTransmitBuffer[bufLoc+1] = f->led[i].blue;
		ledTransmitBuffer[bufLoc+2] = f->led[i].green;
		ledTransmitBuffer[bufLoc+3] = f->led[i].red;
	}

	//setIndicatorLEDs();

	//send out the buffer
	sendToSPILEDs(onboardSPIInst, &ledTransmitBuffer[0], SPI_FRAME_SIZE);
}
