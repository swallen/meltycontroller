
#include "gpio.h"

void initGPIO(GPIO_TypeDef * port, uint8_t pin, uint8_t mode, uint8_t ot, uint8_t speed, uint8_t pull, uint8_t alternate) {
	port->MODER   = (port->MODER   & ~(3 << 2*pin)) | (mode << 2*pin);
	port->OTYPER  = (port->OTYPER  & ~(1 << pin))   | (ot << pin);
	port->OSPEEDR = (port->OSPEEDR & ~(3 << 2*pin)) | (speed << 2*pin);
	port->PUPDR   = (port->PUPDR   & ~(3 << 2*pin)) | (pull << 2*pin);
	if(pin >= 8) {
		port->AFR[1] = (port->AFR[1] & ~(0xF << 4*(pin-8))) | (alternate << 4*(pin-8));
	} else {
		port->AFR[0] = (port->AFR[0] & ~(0xF << 4*pin))     | (alternate << 4*pin);
	}
}

void setGPIO(GPIO_TypeDef * port, uint8_t pin, uint8_t value) {
	if(value) {
		port->ODR |= 1 << pin;
	} else {
		port->ODR &= ~(1 << pin);
	}
}

uint8_t getGPIO(GPIO_TypeDef * port, uint8_t pin) {
	return (port->IDR >> pin) & 0x01;
}
