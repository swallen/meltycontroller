/*
 * topLoop.c
 *
 * this contains the main state machine logic for the bot
 *
 *  Created on: Mar 8, 2019
 *      Author: swallen
 */

#include "topLoop.h"

uint8_t mainState = 0;//we set to an invalid state so the loop is forced to properly set up in safe state

uint32_t jukeStartTime = 0;

uint32_t loopTime = 0;//in us
uint32_t lastMotorSendTime = 0;

uint8_t stickSwitchDebounce = 0;

uint32_t led_blink_time = 0;
uint8_t led_blink = 0;

uint8_t detectedOrientation = 0;

void loop() {

	//INFINITE LOOP
	while(1) {
		feedWatchdog();//bark bark
		loopTime = getMicroseconds();

		receiveSerial();

		//make sure we are still connected to the controller
		if(!isControlled() && mainState != STATE_SAFE){
			setState(STATE_SAFE);
		}

		//switch debouncing functions
		if(stickSwitchDebounce && !getStickSwitch()) stickSwitchDebounce = 0;

		switch(mainState) {
		case(STATE_SAFE):
			//This state is for fault conditions, and for when we don't have communication with the controller.
			if(isControlled()) setState(STATE_IDLE);
		getPhotoDiode();
			break;

		case(STATE_IDLE):
			//this state is when we have communications with the controller, but it's telling us to stay disabled
			if(isEnabled()) {
#ifdef HALO
				if(commandedThrottle() > 10) {
					setState(STATE_SPIN);
				} else {
					setState(STATE_DRIVE);
				}
#endif
#ifdef HITNSPIN
				setState(STATE_SPIN);
#endif
			}

			if(getStickSwitch() && !stickSwitchDebounce) {
				stickSwitchDebounce = 1;
#ifdef HALO	 //HITNSPIN Has no Juke Mode
				setState(STATE_PREJUKE);
#endif
			}

#ifdef MC_DSHOT
			//make sure we don't go *too* fast so the motor controllers don't freak out
			if(loopTime - lastMotorSendTime > MIN_MOTOR_TIME) {
				lastMotorSendTime = loopTime;

				setMotorSpeed2(MOTOR1, 0);
				setMotorSpeed2(MOTOR2, 0);  //For safety in HITNSPIN, Always set Motor 2 to 0
			}
#endif

			detectedOrientation = runOrientationDetect();

			if(getMilliseconds() - led_blink_time > 1000) {
				if(led_blink) {
					setIndicatorLEDs(25, 25, 0);
				} else {
					setIndicatorLEDs(75, 75, 0);
				}
				led_blink = !led_blink;
				led_blink_time = getMilliseconds();
			}
			break;

		case(STATE_PREJUKE):
			//this state is like idle, but we are going to do a juke maneuver if we go to spin mode next
			if(isEnabled()) {
#ifdef HALO
				if(commandedThrottle() > 10) {

					jukeStartTime = getMicroseconds();
					setState(STATE_JUKE);

				} else {
					setState(STATE_DRIVE);
				}
#endif
#ifdef HITNSPIN //No Juke Mode here.
				setState(STATE_DRIVE);
#endif
			}

			if(getStickSwitch() && !stickSwitchDebounce) {
				stickSwitchDebounce = 1;
				setState(STATE_IDLE);
			}

#ifdef MC_DSHOT
			//make sure we don't go *too* fast so the motor controllers don't freak out
			if(loopTime - lastMotorSendTime < MIN_MOTOR_TIME) break;
			lastMotorSendTime = loopTime;

			setMotorSpeed2(MOTOR1, 0);
			setMotorSpeed2(MOTOR2, 0);  //For safety in HITNSPIN, Always set Motor 2 to 0
#endif
			break;

		case(STATE_JUKE):
			if(getMicroseconds() - jukeStartTime > 500000) {
				setState(STATE_SPIN);
			}

#ifdef MC_DSHOT
			//make sure we don't go *too* fast so the motor controllers don't freak out
			if(loopTime - lastMotorSendTime < MIN_MOTOR_TIME) break;
			lastMotorSendTime = loopTime;

			setMotorSpeed(MOTOR1, 400, MOTOR_DIR_CW);

			setMotorSpeed(MOTOR2, 400, MOTOR_DIR_CCW);
#endif
			break;

		case(STATE_DRIVE):
			//this state drives the robot in standard arcade mode
			if(!isEnabled()) setState(STATE_IDLE);
			if(commandedThrottle() > 10) {
				setState(STATE_SPIN);
				continue;
			}


#ifdef MC_DSHOT
			//make sure we don't go *too* fast so the motor controllers don't freak out
			if(loopTime - lastMotorSendTime < MIN_MOTOR_TIME) break;
			lastMotorSendTime = loopTime;
#endif

			setMotorSpeed2(MOTOR1, getThumbX() - getThumbY()/2);
			setMotorSpeed2(MOTOR2, getThumbX() + getThumbY()/2);
			break;

		case(STATE_SPIN):
			//this state drives the robot in meltybrain mode
			if(!isEnabled()) setState(STATE_IDLE);
#ifdef HALO
			if(commandedThrottle() <= 10) {
				setState(STATE_DRIVE);
				continue;
			}
#endif

#ifdef SENSE_ACCEL_BEACON
			runHybridSensing();
			int16_t heading = getHybridAngle();
			uint16_t curSpeed = (uint16_t) (166667/getUsPerDegAccel());
#endif

#ifdef SENSE_BEACON
			runBeacon();
			int16_t heading = getBeaconAngle();
#endif

#ifdef SENSE_ACCEL
			runAccelerometer();
			int16_t heading = getAccelAngle();
			uint16_t curSpeed = (uint16_t) (166667/getUsPerDegAccel());

			//heading -= getCorrection();
			if(heading < 0) heading += 360;
			heading %= 360;
#endif

#ifdef CALIBRATE_ACCELEROMETER
			runBeacon();
			runAccelerometer();
			int16_t heading = getBeaconAngle();
#endif

			//runPOVAnimation(heading);

			//toggle the heading indication LEDs
			if(heading < 180) {
				setIndicatorLEDs(100,0,0);
			} else {
				setIndicatorLEDs(0,100,0);
			}

			//MOTOR COMMAND
#ifdef MC_DSHOT
			//make sure we don't go *too* fast so the motor controllers don't freak out
			if(loopTime - lastMotorSendTime < MIN_MOTOR_TIME) break;
			lastMotorSendTime = loopTime;
#endif

			uint16_t reqSpeed = commandedThrottle() + 400;
			int16_t power = 0;
			//uint8_t direction = getDirSwitch();
			uint8_t direction = detectedOrientation;

			if(curSpeed < reqSpeed) {
				power = (reqSpeed - curSpeed);
			}

			if(power > curSpeed) {
				power = curSpeed < 30 ? 30 : curSpeed;
			}

			//first check if the melty throttle is high enough for translation
			if (getMeltyThrottle() > 10) {

				//calculate the distance between the current heading and the commanded direction
				int16_t diff = 180 - abs(abs(getMeltyAngle() - heading) - 180);
				if(diff < 10) setIndicatorLEDs(0,0,100);

				//now check if we are pointed more towards the commanded direction or the opposite
				if(diff < 90) {
				  //we are pointing towards the commanded heading, forward pulse
				  setMotorSpeed(MOTOR1, power-40, direction);
#ifdef HALO
				  setMotorSpeed(MOTOR2, power+40, direction);
#endif
				} else {
				  //we are pointing opposite the commanded heading, reverse pulse
				  setMotorSpeed(MOTOR1, power+40, direction);
#ifdef HALO
				  setMotorSpeed(MOTOR2, power-40, direction);
#endif
				}
			} else {
				//if we aren't translating, just run the motors at the throttle speed
				setMotorSpeed(MOTOR1, power, direction);
#ifdef HALO
				setMotorSpeed(MOTOR2, power, direction);
#endif
			}

			break;

		default:
			setState(STATE_SAFE);
			break;
		}

		if(mainState != STATE_SPIN) {
			//runTimeAnimation();
		}
	}
}

/* setState()
 * arguments:
 * 	newState: the new state that you wish to transition the bot into
 * returns:
 *  nothing
 *
 *  Use this to change what state the bot is in. This function handles all relevant re-initialization
 *  for every state.
 */

void setState(uint8_t newState) {
	switch(newState) {
	default:


	case(STATE_SAFE):
		mainState = newState;
#if defined(MC_ONESHOT125)
		disableMotors();
#endif
		setIndicatorLEDs(100, 0, 0);
		break;


	case(STATE_IDLE):
		mainState = newState;
#ifdef MC_ONESHOT125
		setMotorSpeed2(MOTOR1, 0);
		setMotorSpeed2(MOTOR2, 0);
#endif
		setIndicatorLEDs(75, 75, 0);
		break;


	case(STATE_PREJUKE):
		mainState = newState;
		setIndicatorLEDs(75, 75, 0);
		break;


	case(STATE_JUKE):
		mainState = newState;

#if defined(MC_ONESHOT125)
		enableMotors();
#endif
		setIndicatorLEDs(0, 100, 0);
		break;


	case(STATE_DRIVE):
		mainState = newState;

#if defined(MC_ONESHOT125)
		enableMotors();
#endif
		setIndicatorLEDs(0, 100, 0);
		break;


	case(STATE_SPIN):
		mainState = newState;

#if defined(MC_ONESHOT125)
		enableMotors();
#endif
		break;


	}

	//assuming the state change was successful, change the animation pattern
	//if(newState == mainState) setAnimation(mainState);
}
