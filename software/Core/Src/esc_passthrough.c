/*
 * esc_passthrough.c
 *
 *  Created on: Nov 1, 2021
 *      Author: swallen
 */

//large parts of this code taken from:
//https://github.com/BrushlessPower/BlHeli-Passthrough

#include "esc_passthrough.h"

uint8_t receivedFromSerial = 0;

uint8_t fromSerialBuf[300];
uint16_t fromSerialIndex = 0;

uint16_t fromSerialPacketLen;

uint8_t toSerialBuf[300];
uint16_t toSerialIndex = 0;

uint8_t fromESCBuf[300];
uint16_t fromESCSize = 0;

uint8_t fourWay_found;

void initPassthrough() {

}

void sendToESC(uint8_t esc, uint8_t tx_buf[], uint16_t buf_size) {

}

uint16_t getFromESC(uint8_t esc, uint8_t rx_buf[], uint16_t timeout) {
	return 0;
}


void runPassthrough() {
	/*
    bool exitEsc = false;
    uint8_t motor_output = escIndex;
	setIndicatorLEDs(0, 0, 0);

    uint32_t escBaudrate = 19200;

    escPort = openEscSerial(motorConfig, ESCSERIAL1, NULL, motor_output, escBaudrate, 0, mode);

    uint8_t ch;
    while (1) {
        if (serialRxBytesWaiting(escPort)) {
        	setIndicatorLEDs(100, 0, 0);
            while (serialRxBytesWaiting(escPort))
            {
                ch = serialRead(escPort);
                serialWrite(escPassthroughPort, ch);
            }
        	setIndicatorLEDs(0, 0, 0);
        }

        if (serialRxBytesWaiting(escPassthroughPort)) {
        	setIndicatorLEDs(0, 100, 0);
            while (serialRxBytesWaiting(escPassthroughPort))
            {
                ch = serialRead(escPassthroughPort);
                exitEsc = processExitCommand(ch);
                if (exitEsc)
                {
                    serialWrite(escPassthroughPort, 0x24);
                    serialWrite(escPassthroughPort, 0x4D);
                    serialWrite(escPassthroughPort, 0x3E);
                    serialWrite(escPassthroughPort, 0x00);
                    serialWrite(escPassthroughPort, 0xF4);
                    serialWrite(escPassthroughPort, 0xF4);
                    closeEscSerial(ESCSERIAL1, mode);
                    return true;
                }
                serialWrite(escPassthroughPort, ch); // blheli loopback

                serialWrite(escPort, ch);
            }
        	setIndicatorLEDs(0, 0, 0);
        }

        delayMilliseconds(5);

    }//while(1)*/
}
