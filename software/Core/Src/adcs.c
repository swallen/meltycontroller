/*
 * photodiode.c
 *
 *  Created on: Mar 8, 2019
 *      Author: swallen
 */

#include "adcs.h"

void initADCs() {
#ifdef HALO
	/**Halo ADC1 GPIO Configuration
	PA2     ------> ADC1_IN2	IR_BEACON_OUT
	PC0     ------> ADC2_IN10	VBATT_MEAS
	*/
	initGPIO(GPIOA, 2, GPIO_MODE_ANALOG, 0, 0, GPIO_PULL_NONE, 0);
	initGPIO(GPIOC, 0, GPIO_MODE_ANALOG, 0, 0, GPIO_PULL_NONE, 0);
#endif
#ifdef HITNSPIN
	/**HitNSpin ADC1 GPIO Configuration
	PA7     ------> ADC1_IN7	IR_BEACON_OUT
	PA6     ------> ADC2_IN6	VBATT_MEAS
	*/
	initGPIO(GPIOA, 7, GPIO_MODE_ANALOG, 0, 0, GPIO_PULL_NONE, 0);
	initGPIO(GPIOA, 6, GPIO_MODE_ANALOG, 0, 0, GPIO_PULL_NONE, 0);
#endif
	//10 bit conversion resolution
	ADC1->CR1 = 0x01000000;
#ifdef HALO
	//15-cycle conversion on channel 2
	ADC1->SMPR2 = 0x00000040;
	//first channel set to 2
	ADC1->SQR3  = 0x00000002;
#endif
#ifdef HITNSPIN
	//15-cycle conversion on channel 7
	ADC1->SMPR2 = 0x00200000;
	//first channel set to 7
	ADC1->SQR3  = 0x00000007;
#endif
	//one channel conversion sequence
	ADC1->SQR1 = 0x00000000;
	//continuous conversion, converter on
	ADC1->CR2 = 0x00000003;
	//start the conversion
	ADC1->CR2 = 0x40000003;

	//10 bit conversion resolution
	ADC2->CR1 = 0x01000000;
#ifdef HALO
	//15-cycle conversion on channel 10
	ADC2->SMPR1 = 0x00000001;
	//first channel set to 10
	ADC2->SQR3  = 0x0000000A;
#endif
#ifdef HITNSPIN
	//15-cycle conversion on channel 6
	ADC2->SMPR1 = 0x00040000;
	//first channel set to 6
	ADC2->SQR3  = 0x00000006;
#endif
	//one channel conversion sequence
	ADC2->SQR1 = 0x00000000;
	//continuous conversion, converter on
	ADC2->CR2 = 0x00000003;
	//start the conversion
	ADC2->CR2 = 0x40000003;
}

uint16_t getVBatt() {
	return (uint16_t) ADC2->DR;
}

uint16_t getPhotoDiode() {
	return (uint16_t) ADC1->DR;
}

#define BEACON_STATE_WAIT 0
#define BEACON_STATE_EDGE 1
#define BEACON_STATE_RECIEVED 2
uint8_t beaconState = BEACON_STATE_WAIT;

uint32_t edgeTime = 0;
uint32_t beaconLastTime = 0;
uint32_t usPerDeg = 0;

#define MAX_USPERDEG 600000UL
#define MIN_BEACON_THRESH 200

void runBeacon() {
	uint16_t beaconMeas = getPhotoDiode();

	switch(beaconState) {
	case BEACON_STATE_WAIT :
		//check for new readings
		if(beaconMeas > MIN_BEACON_THRESH) {
			beaconState = BEACON_STATE_EDGE;
			edgeTime = getMicroseconds();
		}
		break;
	case BEACON_STATE_EDGE:
		//double check the packet
		if(beaconMeas > MIN_BEACON_THRESH) { //if the beacon is still high
			if(getMicroseconds() - edgeTime > 2000UL) {//and 0.2ms has passed
				usPerDeg = (edgeTime - beaconLastTime) / 360UL;//we report a real edge
				beaconLastTime = edgeTime;
				if(usPerDeg >= MAX_USPERDEG) usPerDeg = 0;
				beaconState = BEACON_STATE_RECIEVED;
			}
		} else {
			beaconState = BEACON_STATE_WAIT;
		}
		break;

	case BEACON_STATE_RECIEVED :
		//check for the current packet to end. Wait at least the amount of time for the bot to complete a half revolution at 1000RPM
		if(beaconMeas < MIN_BEACON_THRESH/2) {
			beaconState = BEACON_STATE_WAIT;
		}
		break;
	default :
		beaconState = BEACON_STATE_WAIT;
		break;
	}

	//reset the algorthim if it's been too long since we've seen an edge
	if(getMicroseconds() - beaconLastTime > 500000UL) usPerDeg = 0;
}

int16_t getBeaconAngle() {
	if(usPerDeg != 0) {
		return (int16_t) ((((getMicroseconds() - beaconLastTime) / usPerDeg) + 180) % 360);
	}
	return 0;
}

uint32_t getUsPerDeg() {
	return usPerDeg;
}
