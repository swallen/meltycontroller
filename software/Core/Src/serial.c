/*
 * serial.c
 *
 *  Created on: Mar 8, 2019
 *      Author: swallen
 */

#include "serial.h"

//if you're changing this, change the enabled irq in the init function, and change the name of the interrupt handler
USART_TypeDef * usart = USART1;

#define SERIAL_RX_SIZE 256
uint8_t serialRxBuf[SERIAL_RX_SIZE];
uint8_t serialReadIndex = 0;;
uint8_t serialWriteIndex = 0;
#define PACKET_LEN 11
uint8_t packet[PACKET_LEN];

//how long we will wait before we go into safe mode
#define RECEIVE_TIMEOUT 1000
uint32_t lastReceived = 0;

#define SERIAL_WAIT 0x01
#define SERIAL_PACKETSTART 0x02
uint8_t serialState = SERIAL_WAIT;
uint8_t bytesRead = 0;

_Bool packetReceived;

uint8_t status = 0;
int16_t thumbX = 0;
int16_t thumbY = 0;
uint16_t throt = 0;
uint16_t correction = 0;
uint8_t enable = 0;

uint16_t meltyThrottle;
int16_t meltyAngle;

void initSerial() {

	/**TIM1 GPIO Configuration
	PB6 (Halo)    ------> USART1_TX
	PA9 (HitNSpin)    ------> USART1_TX
	PB7     ------> USART1_RX
	*/
#ifdef HALO
	initGPIO(GPIOB, 6, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_LOW, GPIO_PULL_NONE, 7);
#endif
#ifdef HITNSPIN
	initGPIO(GPIOA, 9, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_LOW, GPIO_PULL_NONE, 7);
#endif
	initGPIO(GPIOB, 7, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_LOW, GPIO_PULL_NONE, 7);

	NVIC_EnableIRQ(USART1_IRQn);
	NVIC_SetPriority(USART1_IRQn, 0);

	uint32_t baudRate = 57600U;
	uint32_t usartdiv = (MAIN_CLOCK/4)*1000000U/(baudRate);
	usart->BRR = ((usartdiv/16) << 4) | (usartdiv%16);
	//USART enabled, RX complete interrupt enabled, receiver enabled
	usart->CR1 = USART_CR1_RE | USART_CR1_RXNEIE | USART_CR1_TE | USART_CR1_UE;
}


uint32_t lastSend = 0;
char sendBuf[11];
void sendResponse() {
	uint16_t vbatt = getVBatt();

#ifdef SENSE_BEACON
	uint32_t speed = getUsPerDeg();
#else
	uint32_t speed = getUsPerDegAccel();
#endif

	sendBuf[0] = 0x7E;
	sendBuf[1] = (uint8_t) (vbatt >> 8);
	sendBuf[2] = (uint8_t) (0x00FF | vbatt);
	sendBuf[3] = (uint8_t) ((speed >> 24) & 0x000000FF);
	sendBuf[4] = (uint8_t) ((speed >> 16) & 0x000000FF);
	sendBuf[5] = (uint8_t) ((speed >> 8) & 0x000000FF);
	sendBuf[6] = (uint8_t) (speed & 0x000000FF);
	sendSerial(sendBuf, 7);
}

void periodicReturnU16(uint16_t value) {
	if(getMilliseconds() - lastSend > 100) {
		lastSend = getMilliseconds();
		//sprintf, snprintf kept giving me hardfaults, so I wrote my own int to string converter
		sendBuf[0] = (value / 1000) + 48;
		sendBuf[1] = ((value % 1000) / 100) + 48;
		sendBuf[2] = ((value % 100) / 10) + 48;
		sendBuf[3] = (value % 10) + 48;
		sendBuf[4] = '\n';

		//turn higher power 0's into spaces
		for(uint8_t i=0; i<3; i++) {
			if(sendBuf[i] == 48) {
				sendBuf[i] = 32;
			} else {
				break;
			}
		}
		sendSerial(sendBuf, 5);
	}
}

void sendBulkU16(uint16_t *data, uint16_t size, uint16_t *dataSent) {
	uint16_t value = data[*dataSent];
	if(getMilliseconds() - lastSend > 10 && (*dataSent) < size) {
		lastSend = getMilliseconds();
		//a header to help align the bytes
		sendBuf[0] = 0x7E;

		//the time
		sendBuf[1] = (uint8_t) ((value >> 8) & 0x00FF);
		sendBuf[2] = (uint8_t) (value & 0x00FF);

		sendSerial(sendBuf, 5);
		(*dataSent)++;
	}
}

void periodicReturnU32(uint32_t value) {
	if(getMilliseconds() - lastSend > 100) {
		lastSend = getMilliseconds();
		//sprintf, snprintf kept giving me hardfaults, so I wrote my own int to string converter
		sendBuf[0] = (value / 1000000000U) + 48;
		for(uint32_t i=1; i<10; i++) {
			sendBuf[10-i] = ((value % int_pow(10,i)) / int_pow(10,i-1)) + 48;
		}
		sendBuf[10] = '\n';

		//turn higher power 0's into spaces
		for(uint8_t i=0; i<9; i++) {
			if(sendBuf[i] == 48) {
				sendBuf[i] = 32;
			} else {
				break;
			}
		}
		sendSerial(sendBuf, 11);
	}
}

//this function is designed to periodically send a packet for calibrating the accelerometer
void periodicReturnCalibration(uint32_t time, uint16_t accel){
	if(getMilliseconds() - lastSend > 100) {
		lastSend = getMilliseconds();
		//a header to help align the bytes
		sendBuf[0] = 0x7E;

		//the time
		sendBuf[1] = (uint8_t) ((time >> 24) & 0x000000FF);
		sendBuf[2] = (uint8_t) ((time >> 16) & 0x000000FF);
		sendBuf[3] = (uint8_t) ((time >> 8) & 0x000000FF);
		sendBuf[4] = (uint8_t) (time & 0x000000FF);

		//the acceleration
		sendBuf[5] = (uint8_t) ((accel >> 8) & 0x00FF);
		sendBuf[6] = (uint8_t) (accel & 0x00FF);

		sendSerial(sendBuf, 7);
	}
}

void USART1_IRQHandler(void) {
	//make sure the interrupt was because data was received
	if(usart->SR & USART_SR_RXNE) {
		serialRxBuf[serialWriteIndex] = usart->DR;
		serialWriteIndex++;
	}
}

void sendSerial(char * buf, uint8_t size) {
	usart->CR1 |= 0x8;
	for(int i=0; i<size; i++) {
		usart->DR = buf[i];
		while(!(usart->SR & USART_SR_TXE));//wait for the byte to be transmitted
	}

	while(!(usart->SR & USART_SR_TC));//wait for the transmission to finish
	usart->CR1 &= ~(0x8);//turn off the transmitter
}

uint8_t charactersInBuffer() {
	return serialReadIndex != serialWriteIndex;
}

uint8_t readByte() {
	uint8_t val = serialRxBuf[serialReadIndex];
	serialReadIndex++;
	return val;
}

void receiveSerial() {
	while(charactersInBuffer()) {
		uint8_t receivedCharacter = readByte();

		if(serialState == SERIAL_WAIT) {
			if(receivedCharacter == 0x7E) {
				serialState = SERIAL_PACKETSTART;
				bytesRead = 0;
			}

			continue;
		}
		packet[bytesRead] = receivedCharacter;
		bytesRead++;

		if(bytesRead >= PACKET_LEN-1) {
			lastReceived = getMilliseconds();
			packetReceived = 1;

			serialState = SERIAL_WAIT;
		}
	}

	//if we received a new packet, crunch the numbers
	if(packetReceived) {
		packetReceived = 0;

    	//unpack the received data into the correct variables
		status = packet[0];
		thumbX = 512 - ((((int16_t) packet[1]) << 8) | ((int16_t) packet[2]));
		thumbY = ((((int16_t) packet[3]) << 8) | ((int16_t) packet[4])) - 512;
		throt = (((uint16_t) packet[5]) << 8) | ((uint16_t) packet[6]);
		correction += (((uint16_t) packet[7]) << 8) | ((uint16_t) packet[8]);
		enable = packet[9];

		correction %= 360;

		uint32_t s32X = (int32_t) thumbX;
		uint32_t s32Y = (int32_t) thumbY;

		//calculate meltybrain commands
		meltyThrottle = (uint16_t) SquareRootRounded(s32X*s32X + s32Y*s32Y)/2;

		meltyAngle = (int16_t) (atan2((double) thumbY, (double) thumbX)*180.0/M_PI) + 90;
		meltyAngle %= 360;
		if(meltyAngle < 0) meltyAngle += 360;

#ifndef CALIBRATE_ACCELEROMETER
		sendResponse();
#endif
	}
}

//helper functions
_Bool isControlled() {
	return (((getMilliseconds() - lastReceived) < RECEIVE_TIMEOUT) && (lastReceived != 0));
}

_Bool isEnabled() {
	return enable == 0xAA;
}

uint16_t commandedThrottle() {
	return throt;
}

uint16_t getMeltyThrottle() {
	return meltyThrottle;
}

int16_t getMeltyAngle() {
	return meltyAngle;
}

int16_t getThumbX() {
	return thumbX;
}

int16_t getThumbY() {
	return thumbY;
}

uint8_t getDirSwitch() {
	return (status & 0x01) > 0;
}

uint8_t getModeSwitch() {
	return (status & 0x02) > 0;
}

uint8_t getStickSwitch() {
	return (status & 0x04) > 0;
}

uint16_t getCorrection() {
	return correction;
}

//an integer square root function shamelessly stolen from
//https://stackoverflow.com/questions/1100090/looking-for-an-efficient-integer-square-root-algorithm-for-arm-thumb2
uint32_t SquareRootRounded(uint32_t a_nInput)
{
    uint32_t op  = a_nInput;
    uint32_t res = 0;
    uint32_t one = 1uL << 30; // The second-to-top bit is set: use 1u << 14 for uint16_t type; use 1uL<<30 for uint32_t type


    // "one" starts at the highest power of four <= than the argument.
    while (one > op)
    {
        one >>= 2;
    }

    while (one != 0)
    {
        if (op >= res + one)
        {
            op = op - (res + one);
            res = res +  2 * one;
        }
        res >>= 1;
        one >>= 2;
    }

    /* Do arithmetic rounding to nearest integer */
    if (op > res)
    {
        res++;
    }

    return res;
}

//an integer exponent function shamelessly stolen from
//https://stackoverflow.com/questions/15394216/integer-power-in-c
unsigned int_pow(int n, unsigned x)
{
    unsigned i, a = n;
    switch (x)
    {
      case 0:
        return 1;
      case 1:
        return n;
      default:
        for (i = 1; i < x; ++i) {
             a *= n;
        }
        break;
    }
    return a;
}
