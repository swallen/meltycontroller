/*
 * motors.c
 *
 *  Created on: Mar 10, 2019
 *      Author: swallen
 */

#include "motors.h"

TIM_TypeDef * timInst = TIM1;
DMA_TypeDef * dmaInst = DMA2;
DMA_Stream_TypeDef * dmaStrInst = DMA2_Stream1;

#ifdef MC_DSHOT
uint16_t motorPulseTrain[18];
#endif

void initMotors() {

	/**TIM1 GPIO Configuration
	PA10        ------> TIM1_CH3
	PA11 (Halo) ------> TIM1_CH4
	*/
	initGPIO(GPIOA, 10, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_LOW, GPIO_PULL_NONE, 1);
#ifdef HALO
	initGPIO(GPIOA, 11, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_LOW, GPIO_PULL_NONE, 1);
#endif

	//setup the timer
	timInst->CR1 &= 0xFFFE; //disable timer
	timInst->CNT = 0; //clear counter

#ifdef MC_ONESHOT125
	timInst->CCMR2 |= 0x6060;//channel 3 & 4 are active when timer < count
	timInst->CCER |= 0x1100;//output compare 3 & 4 outputs are enabled
	timInst->ARR = ONESHOT_PERIOD;

	timInst->CR1 |= 0x0084;//enable timer, prescaler=1, auto-reload enabled
#endif

#ifdef MC_DSHOT
	//make sure the start and end are set to 0
	//The first packet is 0 because for some reason the first byte wasn't being transmitted in full
	//the last packet is 0 to make sure the line stays low when we aren't transmitting a code
	motorPulseTrain[0] = 0;
	motorPulseTrain[17] = 0;

	//disable the dma stream
	dmaStrInst->CR &= 0xFFFFFFFE;
	//wait until the stream is disabled
	while(dmaStrInst->CR & 0x00000001);

	timInst->DIER |= 0x0200; //enable DMA requests on CH1 (no interrupts enabled)
	//timInst->DIER = 0x0100;//send dma request on update
	timInst->CCMR1 |= 0x0010;//channel 1 active when the timer matches
	timInst->CCMR2 |= 0x6868;//channel 3 & 4 are active when timer < count
	timInst->CCER |= 0x1101;//output compare 1, 3 & 4 outputs are enabled

	timInst->BDTR |= 0xA000;

	timInst->ARR = DSHOT_PERIOD; //sets the frequency to the DSHOT baud rate

	//set the DMA trigger channels to fire right before the time rolls over
	timInst->CCR1 = DSHOT_PERIOD - 7;

	//setup the DMA on the peripheral side
	timInst->DCR |= 0x000F;//one transfer, starting from CCR3

	//setup the DMA peripheral
	dmaStrInst->CR = 0x0C032C40;//channel 6, very high priority, size 4 peripheral offset,
	//half word data sizes, memory increment, memory-to-peripheral

	dmaStrInst->M0AR = (uint32_t) motorPulseTrain;//set the peripheral address

	NVIC_EnableIRQ(DMA2_Stream1_IRQn);
	NVIC_SetPriority(DMA2_Stream1_IRQn, 0);

	//enable the timer
	timInst->CR1 |= 0x0085; //enable timer, prescaler=1, auto-reload enabled, one pulse mode
#endif
}

#ifdef MC_ONESHOT125
void disableMotors() {
	timInst->CR1 &= 0xFFFE; //disable timer
}

void enableMotors() {
	timInst->CR1 |= 0x0001;//enable timer
}
#endif

void setMotorSpeed(uint8_t motor, uint16_t speed, uint8_t dir) {
	if(speed > SOFT_MAX_POWER) speed = SOFT_MAX_POWER;
	if(speed < MOTOR_DEADZONE) {
		issueDshotCommand(motor, 0);
		return;
	}

#ifdef MC_DSHOT

#define DSHOT_RANGE 999U

	//convert the speed value to be between 0 and 1000
	speed = ((uint32_t) speed * DSHOT_RANGE) / MOTOR_MAX_POWER;
	if(speed > DSHOT_RANGE ) speed = DSHOT_RANGE;

	//adjust for the direction
	if(dir == MOTOR_DIR_CCW) {
		speed = 1001 + speed;
	} else {
		//speed = DSHOT_RANGE - speed;
	}

	//add 48, as values 1-47 are reserved for special commands
	speed += 48;

	issueDshotCommand(motor, speed);
#endif

#ifdef MC_ONESHOT125
	speed = ((uint32_t) speed * 2500U) / MOTOR_MAX_POWER;

	//adjust for the direction
	if(dir == MOTOR_DIR_CW) {
		speed = 7500 + speed;
	} else {
		speed = 7500 - speed;
	}

	if(motor == MOTOR1) {
		timInst->CCR3 = speed;
	} else if(motor == MOTOR2) {
		timInst->CCR4 = speed;
	}
#endif
}

//helper function that accepts -MAX_SPEED to MAX_SPEED instead of a separate direction field
void setMotorSpeed2(uint8_t motor, int16_t speed) {
	uint8_t dir;
	if(speed > 0) {
		dir = MOTOR_DIR_CW;
	} else {
		speed = -1*speed;
		dir = MOTOR_DIR_CCW;
	}

	setMotorSpeed(motor, (uint16_t) speed, dir);
}

#ifdef MC_DSHOT
void armMotors() {
	delayMilliseconds(500);
	issueDshotCommand(MOTOR1, 2047);
	issueDshotCommand(MOTOR2, 2047);
	delayMilliseconds(500);
	issueDshotCommand(MOTOR1, 49);
	issueDshotCommand(MOTOR2, 49);
}

void issueDshotCommand(uint8_t motor, uint16_t command) {

	//compute the checksum. xor the three nibbles of the speed + the telemetry bit (not used here)
	uint16_t checksum = 0;
	uint16_t checksum_data = command << 1;
	for(uint8_t i=0; i < 3; i++) {
		checksum ^= checksum_data;
		checksum_data >>= 4;
	}
	checksum &= 0x000F;//we only use the least-significant four bits as checksum
	uint16_t dshot_frame = (command << 5) | checksum; //add in the checksum bits to the least-four-significant bits

	//wait until the stream is disabled
	while(dmaStrInst->CR & 0x00000001);

	//Convert the bits to pulse lengths, then write the pulse lengths into the buffer
	for(int i=1; i<17; i++) {
		motorPulseTrain[i] = dshot_frame & 0x8000 ? DSHOT_1_TIME : DSHOT_0_TIME;
		dshot_frame <<= 1;
	}

	//set the peripheral address to the correct channel
	if(motor == MOTOR1) {
		dmaStrInst->PAR = (uint32_t) &(timInst->CCR3);
	}
	else if(motor == MOTOR2) {
		dmaStrInst->PAR = (uint32_t) &(timInst->CCR4);
	} else {
		return;//if the motor selection is invalid, just give up
	}

	//start the dma transfer
	dmaInst->HIFCR = 0x0F7D0F7D;//clear all of the flags because I'm lazy
	dmaInst->LIFCR = 0x0F7D0F7D;
	dmaStrInst->NDTR = 18;//set transfer size to 18 bytes
	dmaStrInst->CR |= 0x00000001;//enable the dma stream

}
#endif
