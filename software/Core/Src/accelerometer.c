/*
 * accelerometer.c
 *
 *  Created on: Mar 10, 2019
 *      Author: swallen
 */

#include "accelerometer.h"

I2C_TypeDef * i2c_accel = I2C3;

int16_t accelVal;

//the minimum accelerometer reading before we attempt to run meltybrain and POV display
#define MELTY_MIN_ACCEL 400

uint32_t lastMeasTime = 0;

uint16_t robotPeriod[2];//measured in microseconds per degree, with some memory for discrete integration

//this is the times we measured the accelerometer at. We keep some history for extrapolation
unsigned long accelMeasTime[2];

#define ACCEL_MEASUREMENT_PERIOD 10//in ms

//this angle (degrees) is calculated only using the accelerometer. We keep it separate to keep our discrete integration algorithms operating smoothly
//the beacon sets our heading to 0, which would mess up the discrete integration if allowed to affect this variable directly
//instead we utilize a trim variable. the beacon controls trim
int16_t accelAngle = 0;

//variables for hybrid mode
uint16_t highestBeaconReading = 0;
int16_t highestBeaconAngle = 0;

int16_t accelTrim = 0;
uint32_t lastTrimTime = 0;

uint16_t angleAtLastMeasurement;

#define ORIENTATION_SMAPLE_COUNT 50
int16_t orientationSample[ORIENTATION_SMAPLE_COUNT];

//for sampling the beacon for debugging the filtering algorithm
#define BEACON_SAMPLE_COUNT 10000
uint16_t beaconSampleIndex = 0;
uint16_t beaconSamples[BEACON_SAMPLE_COUNT];

#define BEACON_DEBUG_ACQUIRE 0
#define BEACON_DEBUG_SEND 1
uint8_t beacon_debug_mode = BEACON_DEBUG_ACQUIRE;

void initI2C() {
	//should be set to the APB1 clock frequency (in MHz)
	i2c_accel->CR2 = 40;

	//fast mode, duty set (clock frequency IS a multiple of 10MHz), CCR=4 (400KHz freq)
	i2c_accel->CCR = (0x3 << 14) | 4;

	//set the maximum rise time to 300ns for fast mode (assuming 40MHz clock)
	//TRISE = 300ns/Tpclk + 1
	i2c_accel->TRISE = 13;

	//enable the peripheral
	i2c_accel->CR1 = 0x00000001;
}

void initAccelerometer() {

	initI2C();

    /**I2C3 GPIO Configuration
    PC9     ------> I2C3_SDA
    PA8     ------> I2C3_SCL
    PB14 (Halo)   ------> accel_int
    PC8 (HitNSpin)   ------> accel_int
    */
	initGPIO(GPIOA, 8, GPIO_MODE_AF, GPIO_OTYPE_OD, GPIO_SPEED_HIGH, GPIO_PULL_UP, 4);
	initGPIO(GPIOC, 9, GPIO_MODE_AF, GPIO_OTYPE_OD, GPIO_SPEED_HIGH, GPIO_PULL_UP, 4);
#ifdef HALO
	initGPIO(GPIOB, 14, GPIO_MODE_INPUT, 0, 0, 0, 0);
#endif

#ifdef HITNSPIN
	initGPIO(GPIOC, 8, GPIO_MODE_INPUT, 0, 0, 0, 0);
#endif

	I2C_ClearBusyFlagErratum();

    //initialize the device registers
    writeI2CReg8Blocking(i2c_accel, I2C_ADDRESS, CTRL_REG1, 0x2F, 1000);

	writeI2CReg8Blocking(i2c_accel, I2C_ADDRESS, CTRL_REG4, 0x30, 1000);
}

void runAccelerometer() {
	//make sure we aren't reading too quickly, the accelerometer only runs at 100Hz
	if(getMilliseconds() - lastMeasTime > ACCEL_MEASUREMENT_PERIOD) {
		lastMeasTime = getMilliseconds();

#ifdef HITNSPIN
		//positive Z
		uint8_t i2c_stat = readI2CReg16Blocking(i2c_accel, I2C_ADDRESS, OUT_Z_L, &accelVal, 10);
#endif
#ifdef HALO
		//negative X
		uint8_t i2c_stat = readI2CReg16Blocking(i2c_accel, I2C_ADDRESS, OUT_X_L, &accelVal, 10);
#endif

	    //shift all of the old values down
	    for(int i=1; i>0; i--) {
	      accelMeasTime[i] = accelMeasTime[i-1];
	    }
	    //put in the new value
	    accelMeasTime[0] = getMicroseconds();

#ifdef HALO
	    accelVal *= 2;//TODO: recalibrate with the full scale
#endif

		if(accelVal < 0) accelVal *= -1;

		if(i2c_stat != 0) {//If we get here, the accelerometer suddenly doesn't work
			//try clearing the busy erratum and reading again next loop
			I2C_ClearBusyFlagErratum();
			lastMeasTime -= ACCEL_MEASUREMENT_PERIOD;
		} else {
			//accelVal has successfully updated with the new accelerometer raw value
#ifdef CALIBRATE_ACCELEROMETER
		    periodicReturnCalibration(getUsPerDeg(), accelVal);
#endif

		    //give up if the bot is moving too slowly
		    if(accelVal < MELTY_MIN_ACCEL) return;

		    //this equation has been carefully calibrated for this bot. See here for explanation:
		    //https://www.swallenhardware.io/battlebots/2018/8/12/halo-pt-9-accelerometer-calibration
		    robotPeriod[1] = robotPeriod[0];
#ifdef HALO
		    //coefficients: 22366.73657117
		    robotPeriod[0] = (uint32_t) (22367 / sqrt((double) (accelVal)));
#endif
#ifdef HITNSPIN
		    //coefficients: 11011.4902
		    robotPeriod[0] = (uint32_t) (11011 / sqrt((double) (accelVal)));
#endif

		    //find the new angle
		    //TRIANGULAR INTEGRATION
		    uint32_t deltaT = accelMeasTime[0] - accelMeasTime[1];
		    if(robotPeriod[0] != 0 && robotPeriod[1] != 0) {
		    	angleAtLastMeasurement = (angleAtLastMeasurement + (deltaT/robotPeriod[0] + deltaT/robotPeriod[1])/2) % 360;

				accelAngle = angleAtLastMeasurement;
			}
		}
	} else {//if it isn't time to check the accelerometer, predict our current heading
		//predict the current velocity by extrapolating old data
		uint32_t newTime = getMicroseconds();
		uint32_t periodPredicted = robotPeriod[1] + (newTime - accelMeasTime[1]) * (robotPeriod[0] - robotPeriod[1]) / (accelMeasTime[0] - accelMeasTime[1]);

		//predict the current robot heading by triangular integration up to the extrapolated point
		uint32_t deltaT = newTime - accelMeasTime[0];
	    if(robotPeriod[0] != 0 && periodPredicted != 0) {
	    	accelAngle = (angleAtLastMeasurement + (deltaT/periodPredicted + deltaT/robotPeriod[0])/2) % 360;
		}
	}
}

uint8_t runOrientationDetect() {
	if(getMilliseconds() - lastMeasTime > ACCEL_MEASUREMENT_PERIOD) {
		lastMeasTime = getMilliseconds();

#ifdef HITNSPIN
		//positive X
		uint8_t i2c_stat = readI2CReg16Blocking(i2c_accel, I2C_ADDRESS, OUT_X_L, &accelVal, 10);
#endif
#ifdef HALO
		//positive Y
		uint8_t i2c_stat = readI2CReg16Blocking(i2c_accel, I2C_ADDRESS, OUT_Y_L, &accelVal, 10);
#endif

		if(i2c_stat != 0) {//If we get here, the accelerometer suddenly doesn't work
			//try clearing the busy erratum and reading again next loop
			I2C_ClearBusyFlagErratum();
			lastMeasTime -= ACCEL_MEASUREMENT_PERIOD;
		} else {
			for(int i=ORIENTATION_SMAPLE_COUNT-1; i>0; i-- ) {
				orientationSample[i] = orientationSample[i-1];
			}

			orientationSample[0] = accelVal-ACCEL_Y_OFFSET;
		}
	}

	int16_t sum = 0;

	for(int i=0; i<ORIENTATION_SMAPLE_COUNT; i++) {
		sum += orientationSample[i];
	}
	sum = sum/ORIENTATION_SMAPLE_COUNT;

	return sum < 0;
}

//this is the angle as kept track solely by the accelerometer integration algorithm. THIS VALUE DRIFTS!
int16_t getAccelAngle() {
	return (int16_t) accelAngle;
}

#define BANDPASS_SAMPLES 20
uint16_t bandPassReadings[BANDPASS_SAMPLES];

void runHybridSensing() {
	runAccelerometer();

	if(upToSpeed()) {
		uint16_t reading = getPhotoDiode();
		if(reading < 1500) {
			for(int i=BANDPASS_SAMPLES-2; i>=0; i--) {
				bandPassReadings[i+1] = bandPassReadings[i];
			}
			bandPassReadings[0] = reading;

			uint32_t sum = 0;
			for(int i=0; i<BANDPASS_SAMPLES; i++) {
				sum += (uint32_t) bandPassReadings[i];
			}

			sum /= BANDPASS_SAMPLES;

			if(sum > highestBeaconReading && sum > 100) {
				highestBeaconReading = sum;
				highestBeaconAngle = getAccelAngle();
			}
		}

		//periodically trim the accelerometer and reset the beacon reading
		if(getMicroseconds() - lastTrimTime > 100000) {
			lastTrimTime = getMicroseconds();
			accelTrim = highestBeaconAngle;
			highestBeaconReading = 0;
		}
	}
}

int16_t getHybridAngle() {
	int16_t angle = getAccelAngle() - accelTrim;
#ifdef HITNSPIN
	angle += 90;
#endif
	if(angle < 0) angle += 360;
	angle = (angle + 180) % 360;
	return angle;
}

uint32_t getUsPerDegAccel() {
	return robotPeriod[0];
}

_Bool upToSpeed() {
	return accelVal > MELTY_MIN_ACCEL;
}

uint8_t writeI2CReg8Blocking(I2C_TypeDef *i2c, uint8_t addr, uint8_t subaddr, uint8_t data, uint32_t timeout) {
	uint32_t startTime = getMilliseconds();
	//make sure the stop bit is cleared
	while(i2c_accel->CR1 & 0x0200) {
		if(startTime + timeout < getMilliseconds()) return 1;
	}

	//send the start bit
	i2c_accel->CR1 |= 0x0100;
	//wait for the start bit to complete
	while(!(i2c_accel->SR1 & 0x0001)) {
		if(startTime + timeout < getMilliseconds()) return 2;
	}

	//load the slave address into the peripheral
	i2c_accel->DR = addr;
	//wait for the address to be sent
	while(!(i2c_accel->SR1 & 0x0002)) {
		if(startTime + timeout < getMilliseconds()) return 3;
	}
	//I2C peripheral needs this status register to be read to continue, but we don't really care about anything in it
	i2c_accel->SR2;

	//load the subaddress byte into the peripheral
	i2c_accel->DR = subaddr;
	//wait for the byte to be sent
	while(!(i2c_accel->SR1 & 0x0080)) {
		if(startTime + timeout < getMilliseconds()) return 4;
	}

	//load the data byte into the peripheral
	i2c_accel->DR = data;
	//wait for the byte to be sent
	while(!(i2c_accel->SR1 & 0x0080)) {
		if(startTime + timeout < getMilliseconds()) return 5;
	}

	//send the stop bit
	i2c_accel->CR1 |= 0x0200;

	return 0;
}

uint8_t readI2CReg16Blocking(I2C_TypeDef *i2c,
		uint8_t addr,
		uint8_t subaddr,
		int16_t *data,
		uint32_t timeout) {
	uint32_t startTime = getMilliseconds();
	//make sure the stop bit is cleared
	while(i2c_accel->CR1 & 0x0200) {
		if(startTime + timeout < getMilliseconds()) return 1;
	}

	//send the start bit
	i2c_accel->CR1 |= 0x0100;
	//wait for the start bit to complete
	while(!(i2c_accel->SR1 & 0x0001)) {
		if(startTime + timeout < getMilliseconds()) return 2;
	}

	//load the slave address into the peripheral
	i2c_accel->DR = addr;
	//wait for the address to be sent
	while(!(i2c_accel->SR1 & 0x0002)) {
		if(startTime + timeout < getMilliseconds()) return 3;
	}
	//I2C peripheral needs this status register to be read to continue, but we don't really care about anything in it
	if(i2c_accel->SR2){};

	//load the subaddress byte into the peripheral. We assert the MSB to indicate multi-byte transfer
	i2c_accel->DR = subaddr | 0x80;
	//wait for the byte to be sent
	while(!(i2c_accel->SR1 & 0x0080)) {
		if(startTime + timeout < getMilliseconds()) return 4;
	}

	//send a repeated start bit
	i2c_accel->CR1 |= 0x0100;
	//wait for the start bit to complete
	while(!(i2c_accel->SR1 & 0x0001)) {
		if(startTime + timeout < getMilliseconds()) return 5;
	}

	//load the slave address into the peripheral. Assert LSB to indicate read operation
	i2c_accel->DR = addr | 0x01;
	//wait for the address to be sent
	while(!(i2c_accel->SR1 & 0x0002)) {
		if(startTime + timeout < getMilliseconds()) return 6;
	}

	//procedure for 2-byte reads from reference manual, RM0390 Pg 770
	//optimized i2c example:
	//https://www.st.com/content/ccc/resource/technical/document/application_note/5d/ae/a3/6f/08/69/4e/9b/CD00209826.pdf/files/CD00209826.pdf/jcr:content/translations/en.CD00209826.pdf
	//"STM32F10xxx I2C optimized examples"

	i2c_accel->CR1 |= 0x0800;//Set POS
	__disable_irq();//disable interrupts
	i2c_accel->SR2;//clear ADDR by reading SR2
	i2c_accel->CR1 &= ~(0x0400);//clear ACK
	__enable_irq();//enable interrupts

	//wait for the bytes to come in (BTF==1)
	while(!(i2c_accel->SR1 & 0x0004)) {
		if(startTime + timeout < getMilliseconds()) return 7;
	}

	__disable_irq();
	i2c_accel->CR1 |= 0x0200;//set STOP
	uint16_t dataL = i2c_accel->DR;//read the lower data byte
	__enable_irq();
	uint16_t dataH = i2c_accel->DR;//read upper data byte

	while(!(i2c_accel->CR1 & 0x0200)) {//wait until STOP is cleared by hardware
		if(startTime + timeout < getMilliseconds()) return 8;
	}

	i2c_accel->CR1 &= ~(0x0800);//clear POS
	i2c_accel->CR1 |= 0x0400;//Set ACK

	*data = (dataH << 8) | dataL;

	return 0;
}

//shamelessly stolen from https://electronics.stackexchange.com/questions/267972/i2c-busy-flag-strange-behaviour
//to fix a permanent busy flag in the i2c peripheral
void I2C_ClearBusyFlagErratum()
{

  // 1. Clear PE bit.
  i2c_accel->CR1 &= ~(0x0001);

  //HAL_I2C_DeInit(&hi2c3);

  //  2. Configure the SCL and SDA I/Os as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR).
  initGPIO(GPIOA, 8, GPIO_MODE_OUTPUT, GPIO_OTYPE_OD, GPIO_SPEED_HIGH, GPIO_PULL_UP, 4);
  setGPIO(GPIOA, 8, 1);
  initGPIO(GPIOC, 9, GPIO_MODE_OUTPUT, GPIO_OTYPE_OD, GPIO_SPEED_HIGH, GPIO_PULL_UP, 4);
  setGPIO(GPIOC, 9, 1);

  // 3. Check SCL and SDA High level in GPIOx_IDR.
  while (!getGPIO(GPIOC, 9))
  {
    asm("nop");
  }

  while (!getGPIO(GPIOA, 8))
  {
    asm("nop");
  }

  // 4. Configure the SDA I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR).
  setGPIO(GPIOC, 9, 0);

  //  5. Check SDA Low level in GPIOx_IDR.
  while (getGPIO(GPIOC, 9))
  {
    asm("nop");
  }

  // 6. Configure the SCL I/O as General Purpose Output Open-Drain, Low level (Write 0 to GPIOx_ODR).
  setGPIO(GPIOA, 8, 0);

  //  7. Check SCL Low level in GPIOx_IDR.
  while (getGPIO(GPIOA, 8))
  {
    asm("nop");
  }

  // 8. Configure the SCL I/O as General Purpose Output Open-Drain, High level (Write 1 to GPIOx_ODR).
  setGPIO(GPIOA, 8, 1);

  // 9. Check SCL High level in GPIOx_IDR.
  while (!getGPIO(GPIOA, 8))
  {
    asm("nop");
  }

  // 10. Configure the SDA I/O as General Purpose Output Open-Drain , High level (Write 1 to GPIOx_ODR).
  setGPIO(GPIOC, 9, 1);

  // 11. Check SDA High level in GPIOx_IDR.
  while (!getGPIO(GPIOC, 9))
  {
    asm("nop");
  }

  // 12. Configure the SCL and SDA I/Os as Alternate function Open-Drain.
  initGPIO(GPIOA, 8, GPIO_MODE_AF, GPIO_OTYPE_OD, GPIO_SPEED_HIGH, GPIO_PULL_UP, 4);
  initGPIO(GPIOC, 9, GPIO_MODE_AF, GPIO_OTYPE_OD, GPIO_SPEED_HIGH, GPIO_PULL_UP, 4);

  // 13. Set SWRST bit in I2Cx_CR1 register.
  i2c_accel->CR1 |= 0x8000;

  asm("nop");

  // 14. Clear SWRST bit in I2Cx_CR1 register.
  i2c_accel->CR1 &= ~0x8000;

  asm("nop");

  // 15. Enable the I2C peripheral by setting the PE bit in I2Cx_CR1 register
  // Call initialization function.
  initI2C();
}

