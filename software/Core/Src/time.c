/*
 * time.c
 *
 *  Created on: Apr 16, 2019
 *      Author: swallen
 */

#include "time.h"

TIM_TypeDef * microTimInst = TIM5;
TIM_TypeDef * milliTimInst = TIM2;
TIM_TypeDef * milliMasterTimInst = TIM4;

void initTimers() {
	//SETUP THE MICROSECONDS TIMER
	//config timer 5 to be our microseconds count
	microTimInst->CR1 = 0;//make sure the timer is disabled

	microTimInst->PSC = (MAIN_CLOCK*2/4)-1;//gives 1MHz, or 1us period
	//tim2Inst->ARR = 0xFFFF;
	microTimInst->CNT = 0;

	//SETUP THE MILLISECONDS TIMER
	//config timer 4 to be our millisecond master timer
	milliMasterTimInst->CR1 = 0;//make sure the timer is disabled
	//the following prescaler relies on a /2 clock division in CR1
	milliMasterTimInst->PSC = (MAIN_CLOCK*2/4)-1;//gives 1MHz, or 1us period
	//tim2Inst->ARR = 0xFFFF;
	milliMasterTimInst->CNT = 0;
	milliMasterTimInst->ARR = 1000;  //Count to 1000 before looping
	milliMasterTimInst->CR2 = (0b010 << TIM_CR2_MMS_Pos); //Configure This timer as a master timer with the TRGO signal output.

	//config timer 2 to be our milliseconds count
	milliTimInst->CR1 = 0;//make sure the timer is disabled
	//the following prescaler relies on a /2 clock division in CR1
	milliTimInst->PSC = 0; //0 Prescaler as this clock is clocked by milliMasterTimInst as a pure counter.
	//tim2Inst->ARR = 0xFFFF;
	milliTimInst->CNT = 0;
	//Enable External Clock 2 Mode for clocking. Table 112 sets TRGO on TS 3.
	milliTimInst->SMCR = (0b011<< TIM_SMCR_TS_Pos) | (0b111<< TIM_SMCR_SMS_Pos);


	//ENABLE THE TIMERS
	milliTimInst->CR1 = 1 | (0x1 << TIM_CR1_CKD_Pos);
	milliMasterTimInst->CR1 = 1 | (0x1 << TIM_CR1_DIR_Pos); //Enable, Count Down
	microTimInst->CR1 = 1;

	//manually generate an update event so the prescaler loads into the shadow registers
	//this took a ridiculous two hours to figure out. Just so you know
	milliTimInst->EGR |= 0x0001;
	milliMasterTimInst->EGR |= 0x0001;
	microTimInst->EGR |= 0x0001;
	resetTimers();
}

uint32_t getMicroseconds() {
	return microTimInst->CNT;
}

uint32_t getMilliseconds() {
	return milliTimInst->CNT;
}

void delayMilliseconds(uint32_t delay) {
	uint32_t starttime = getMilliseconds();
	while(getMilliseconds() - starttime < delay);
}

void delayMicroseconds(uint32_t delay) {
	uint32_t starttime = getMicroseconds();
	while(getMicroseconds() - starttime < delay);
}

void resetTimers() {
	microTimInst->CNT = 0;
	milliTimInst->CNT = 0;
}

