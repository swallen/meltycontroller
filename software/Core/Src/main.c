#include "main.h"

#define VECT_TAB_OFFSET  0x00 /*!< Vector Table base offset field.
                                   This value must be a multiple of 0x200. */

int _write(int file, char *ptr, int len) {
  /* Implement your write code here, this is used by puts and printf for example */
  for (int i = 0; i < len; i++)
    ITM_SendChar((*ptr++));
  return len;
}

void SystemInit() {
	//Chip powerup sequence
	SCB->VTOR = FLASH_BASE | VECT_TAB_OFFSET; /* Vector Table Relocation in Internal FLASH */
	//enable the FPU
	SCB->CPACR |= ((3UL << 10*2)|(3UL << 11*2));  /* set CP10 and CP11 Full Access */

	//Make sure HSI is selected
	RCC->CFGR |= RCC_CFGR_SW_HSI;
	while((RCC->CFGR & RCC_CFGR_SWS_Msk) != RCC_CFGR_SWS_HSI);//wait until HSI is selected
	//Make sure PLL is disabled
	RCC->CR &= ~RCC_CR_PLLON;

	//enable the power control peripheral
	RCC->APB1ENR |= RCC_APB1ENR_PWREN;

	//select voltage scale 1 to allow highest frequencies
	//PWR->CR = (3 << PWR_CR_VOS_Pos) | PWR_CR_VOS;
	//while(!(PWR->CSR & PWR_CSR_VOSRDY));//wait until the new scaling is ready


#if MAIN_CLOCK!=160
#error You didnt check the clk divider settings after changing the main clock frequency like a scrub
#endif
	uint32_t pll_r = 2;//divides the PLL clock by this value. Not used for anything right now
	uint32_t pll_q = 8;//divides the PLL clock by this value. Should create 48MHz or less (exactly 48MHz for USB on-the-go)
	uint32_t pll_p = 0;//divides output by 2. used for MAIN_CLOCK
	uint32_t pll_n = 2*MAIN_CLOCK;//multiplies the PLL input frequency by this value to create the PLL clock. 100MHz<x<438MHz
	uint32_t pll_m = 16;//divider to make the PLL input frequency 1MHz

	RCC->PLLCFGR = (pll_r << RCC_PLLCFGR_PLLR_Pos) |
					(pll_q << RCC_PLLCFGR_PLLQ_Pos) |
					(pll_p << RCC_PLLCFGR_PLLP_Pos) |
					(pll_n << RCC_PLLCFGR_PLLN_Pos) |
					(pll_m << RCC_PLLCFGR_PLLM_Pos);

	//configure the dividers to max to make sure we don't exceed spec
	RCC->CFGR |= RCC_CFGR_PPRE2_DIV16 | RCC_CFGR_PPRE1_DIV16 | RCC_CFGR_HPRE_DIV512;

	//Enable PLL
	RCC->CR |= RCC_CR_PLLON | (0x10 << RCC_CR_HSITRIM_Pos);

	PWR->CR |= PWR_CR_ODEN;//enable overdrive mode to allow the highest frequencies
	while(!(PWR->CSR & PWR_CSR_ODRDY));//wait for overdrive to be ready
	PWR->CR |= PWR_CR_ODSWEN;//switch to overdrive mode
	while(!(PWR->CSR & PWR_CSR_ODSWRDY));//wait for the switch to finish

	//the flash wait state settings here are good for 2.7V-3.6V range. See datasheet RM0390 v6 pg. 66
#if MAIN_CLOCK > 150
	uint32_t flash_wait = FLASH_ACR_LATENCY_5WS;
#elif MAIN_CLOCK > 120
	uint32_t flash_wait = FLASH_ACR_LATENCY_4WS;
#elif MAIN_CLOCK > 90
	uint32_t flash_wait = FLASH_ACR_LATENCY_3WS;
#elif MAIN_CLOCK > 60
	uint32_t flash_wait = FLASH_ACR_LATENCY_2WS;
#elif MAIN_CLOCK > 30
	uint32_t flash_wait = FLASH_ACR_LATENCY_1WS;
#else
	uint32_t flash_wait = FLASH_ACR_LATENCY_0WS;
#endif

	//prefetch enable, icache enable, dcache enable
	FLASH->ACR |= FLASH_ACR_PRFTEN | FLASH_ACR_ICEN | FLASH_ACR_DCEN | flash_wait;

	//wait until the PLL is locked
	while(!(RCC->CR & RCC_CR_PLLRDY));

	//command the main clock to switch to the PLL
	//apb1=ahb_clock/PPRE1 (SHOULD NOT EXCEED 45MHz)
	//apb2=ahb_clock/PPRE2 (SHOULD NOT EXCEED 90MHz)
	RCC->CFGR = RCC_CFGR_SW_PLL | RCC_CFGR_PPRE2_DIV4 | RCC_CFGR_PPRE1_DIV4 | RCC_CFGR_HPRE_DIV1 | (6 << RCC_CFGR_MCO2PRE_Pos);

	//wait until the clock switch is complete
	while((RCC->CFGR & RCC_CFGR_SWS_Msk) != RCC_CFGR_SWS_PLL);
}

int main(void)
{

	RCC->AHB1ENR = 	RCC_AHB1ENR_DMA2EN |
					RCC_AHB1ENR_GPIOAEN |
					RCC_AHB1ENR_GPIOBEN |
					RCC_AHB1ENR_GPIOCEN;

	RCC->APB1ENR = 	RCC_APB1ENR_I2C3EN |
					RCC_APB1ENR_SPI3EN |
					RCC_APB1ENR_SPI2EN |
					RCC_APB1ENR_TIM5EN |
					RCC_APB1ENR_TIM4EN |
					RCC_APB1ENR_TIM3EN |
					RCC_APB1ENR_TIM2EN;

	RCC->APB2ENR = 	RCC_APB2ENR_SPI1EN |
					RCC_APB2ENR_ADC1EN |
					RCC_APB2ENR_ADC2EN |
					RCC_APB2ENR_USART1EN |
					RCC_APB2ENR_TIM1EN;

	//wait a little to avoid enabling peripherals too fast after clock enable. See errata sheet.
	for(int i=0; i<10; i++) asm("NOP");

	//This configures PC9 to output sysclock/5 for debugging via MCO2
	//The pin's fmax is limited by capacitive loading and voltage, see datasheet (DS10693 v10 pg. 120)
	//initGPIO(GPIOC, 9, GPIO_MODE_AF, GPIO_OTYPE_PP, GPIO_SPEED_HIGH, GPIO_PULL_NONE, 0);

	initTimers();
	LED_init();
	initSerial();

#ifndef ESC_PASSTHROUGH
	//initAnimations();
	//setAnimation(STATE_SAFE);
	initADCs();
	initWatchdog();
	initMotors();
	initAccelerometer();


	setIndicatorLEDs(50, 0, 50);
	feedWatchdog();
	delayMilliseconds(100);
	setIndicatorLEDs(0, 50, 0);
	feedWatchdog();
	delayMilliseconds(100);
	setIndicatorLEDs(50, 0, 50);
	feedWatchdog();
	delayMilliseconds(100);
	setIndicatorLEDs(0, 50, 0);
	feedWatchdog();
	delayMilliseconds(100);

	loop();
#else

	initPassthrough();

	runPassthrough();

#endif
}
