/*
 * watchdog.c
 *
 *  Created on: May 19, 2019
 *      Author: swallen
 */

#include "watchdog.h"

void initWatchdog() {
	//500ms timeout (time=reload*prescaler/32000)
	IWDG->PR = 0x3;//prescaler=32
	IWDG->RLR = 500;//reload value
	IWDG->KR = 0xCCCC;//start the watchdog
}

void feedWatchdog() {
	IWDG->KR = 0xAAAA;
}
