/*
 * esc_passthrough.h
 *
 *  Created on: Nov 1, 2021
 *      Author: swallen
 */

#ifndef INC_ESC_PASSTHROUGH_H_
#define INC_ESC_PASSTHROUGH_H_

#include "stm32f446xx.h"
#include "serial.h"
#include "time.h"
#include "LEDs.h"

void initPassthrough();
void sendToESC(uint8_t esc, uint8_t tx_buf[], uint16_t buf_size);
uint16_t getFromESC(uint8_t esc, uint8_t rx_buf[], uint16_t timeout);
void runPassthrough();

#endif /* INC_ESC_PASSTHROUGH_H_ */
