/*
 * gpio.h
 *
 *  Created on: May 14, 2021
 *      Author: swallen
 */

#ifndef GPIO_H
#define GPIO_H

#include "stm32f446xx.h"

#define GPIO_MODE_INPUT  0x0
#define GPIO_MODE_OUTPUT 0x01
#define GPIO_MODE_AF     0x2
#define GPIO_MODE_ANALOG 0x3

#define GPIO_OTYPE_PP 0x0
#define GPIO_OTYPE_OD 0x1

#define GPIO_SPEED_LOW    0x0
#define GPIO_SPEED_MEDIUM 0x1
#define GPIO_SPEED_FAST   0x2
#define GPIO_SPEED_HIGH   0x3

#define GPIO_PULL_NONE 0x0
#define GPIO_PULL_UP   0x1
#define GPIO_PULL_DOWN 0x2

void initGPIO(GPIO_TypeDef * port, uint8_t pin, uint8_t mode, uint8_t ot, uint8_t speed, uint8_t pull, uint8_t alternate);
void setGPIO(GPIO_TypeDef * port, uint8_t pin, uint8_t value);
uint8_t getGPIO(GPIO_TypeDef * port, uint8_t pin);

#endif /* GPIO_H */
