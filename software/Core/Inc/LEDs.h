/*
 * LEDs.h
 *
 *  Created on: Feb 24, 2019
 *      Author: swallen
 */

#ifndef LEDS_H_
#define LEDS_H_

#include <stdlib.h>
#include "stm32f446xx.h"
#include "gpio.h"
#include "params.h"

#define APA102_HEADER_SIZE 4
#define APA102_DATA_SIZE 4
#define APA102_TRAILER_SIZE 1

#define NUM_SERIAL_LEDS 5

#define SPI_FRAME_SIZE (APA102_HEADER_SIZE + APA102_DATA_SIZE*NUM_SERIAL_LEDS + APA102_TRAILER_SIZE)
typedef struct __Frame {
	uint16_t duration;//in half-degrees or milliseconds
	struct {
		uint8_t red;
		uint8_t green;
		uint8_t blue;
	} led[NUM_SERIAL_LEDS];
	struct __Frame *next;
} *Frame_P, Frame;

extern void LED_init(void);
void sendToSPILEDs(SPI_TypeDef * inst, uint8_t * buf, uint8_t size);
extern void setIndicatorLEDs(uint16_t red, uint16_t green, uint16_t blue);
extern void updateLEDs(Frame_P f);

#endif /* LEDS_H_ */
