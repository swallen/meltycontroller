#ifndef __MAIN_H
#define __MAIN_H

#include "topLoop.h"
#include "stm32f446xx.h"
#include "gpio.h"
#include "params.h"
#include "watchdog.h"
#include "animations.h"
#include "serial.h"
#include "adcs.h"
#include "motors.h"
#include "accelerometer.h"
#include "time.h"
#ifdef ESC_PASSTHROUGH
#include "esc_passthrough.h"
#endif

int _write(int file, char *ptr, int len);
void SystemInit();

int main(void);

#endif
