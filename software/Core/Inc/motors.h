/*
 * motors.h
 *
 *  Created on: Mar 10, 2019
 *      Author: swallen
 */

#ifndef MOTORS_H_
#define MOTORS_H_

#include "stm32f446xx.h"
#include "gpio.h"
#include "time.h"
#include "params.h"

#define MC_DSHOT
//#define MC_ONESHOT125

#define MOTOR1 1
#define MOTOR2 2

#define MOTOR_DIR_CW 0
#define MOTOR_DIR_CCW 1
#define MOTOR_BRAKE_ON 0
#define MOTOR_BRAKE_OFF 1

#define MOTOR_MAX_POWER 512
#define SOFT_MAX_POWER 250

#define MOTOR_DEADZONE 100//the minimum speed value before the motors are engaged

void initMotors(void);

void setMotorSpeed(uint8_t motor, uint16_t speed, uint8_t dir);
void setMotorSpeed2(uint8_t motor, int16_t speed);

#ifdef MC_DSHOT
//all of these values are tuned for a base peripheral clock of 80,000,000Hz
	//#define DSHOT300
	//#define DSHOT600
	#define DSHOT1200

	#ifdef DSHOT300
		#define DSHOT_PERIOD 264
		#define DSHOT_1_TIME 208
		#define DSHOT_0_TIME 104
		#define MIN_MOTOR_TIME 400UL
	#endif

	#ifdef DSHOT600
		#define DSHOT_PERIOD 132
		#define DSHOT_1_TIME 104 //75% of 600 baud, tuned for 1250ns
		#define DSHOT_0_TIME 52 //37% of 600 baud, tuned for 625ns
		#define MIN_MOTOR_TIME 200UL//in us
	#endif

	#ifdef DSHOT1200
		#define DSHOT_PERIOD 66
		#define DSHOT_1_TIME 52
		#define DSHOT_0_TIME 26
		#define MIN_MOTOR_TIME 100UL
	#endif

//this typedef taken from src\main\drivers\pwm_output.h in the betaflight github page
//They may be different for different ESCs!
typedef enum {
    DSHOT_CMD_MOTOR_STOP	= 0,
    DSHOT_CMD_BEACON1		= 1,
    DSHOT_CMD_BEACON2		= 2,
    DSHOT_CMD_BEACON3		= 3,
    DSHOT_CMD_BEACON4		= 4,
    DSHOT_CMD_BEACON5		= 5,
    DSHOT_CMD_ESC_INFO		= 6, // V2 includes settings
    DSHOT_CMD_SPIN_DIRECTION_1	= 7,
    DSHOT_CMD_SPIN_DIRECTION_2	= 8,
    DSHOT_CMD_3D_MODE_OFF	= 9,
    DSHOT_CMD_3D_MODE_ON	= 10,
    DSHOT_CMD_SETTINGS_REQUEST	= 11, // Currently not implemented
    DSHOT_CMD_SAVE_SETTINGS	= 12,
    DSHOT_CMD_SPIN_DIRECTION_NORMAL		= 20,
    DSHOT_CMD_SPIN_DIRECTION_REVERSED	= 21,
    DSHOT_CMD_LED0_ON		= 22, // BLHeli32 only
    DSHOT_CMD_LED1_ON		= 23, // BLHeli32 only
    DSHOT_CMD_LED2_ON		= 24, // BLHeli32 only
    DSHOT_CMD_LED3_ON		= 25, // BLHeli32 only
    DSHOT_CMD_LED0_OFF		= 26, // BLHeli32 only
    DSHOT_CMD_LED1_OFF		= 27, // BLHeli32 only
    DSHOT_CMD_LED2_OFF		= 28, // BLHeli32 only
    DSHOT_CMD_LED3_OFF		= 29, // BLHeli32 only
    DSHOT_CMD_AUDIO_STREAM_MODE_ON_OFF	= 30, // KISS audio Stream mode on/Off
    DSHOT_CMD_SILENT_MODE_ON_OFF		= 31, // KISS silent Mode on/Off
    DSHOT_CMD_SIGNAL_LINE_TELEMETRY_DISABLE	= 32,
    DSHOT_CMD_SIGNAL_LINE_CONTINUOUS_ERPM_TELEMETRY	= 33,
    DSHOT_CMD_MAX			= 47
} dshotCommands_e;

void issueDshotCommand(uint8_t motor, uint16_t command);
void armMotors(void);
#endif

#ifdef MC_ONESHOT125
#define ONESHOT_PERIOD 10000
void disableMotors(void);
void enableMotors(void);
#endif


#endif /* MOTORS_H_ */
