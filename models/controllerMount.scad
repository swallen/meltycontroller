fid = 100;

channelD = 118.45*2;
ringID = channelD - (6*2);

ringHeight = 44;
channelZDisp = 6.5;

mountLength = 70;
mountWidth = 25;//this does not include the round bulge into the channel
            
arcHalfLength = atan2(mountLength/2, channelD/2);
mountXDisp = (channelD/2)*cos(arcHalfLength)-mountWidth;

pcbBounds = [8, 50, 36];

ringBoltYDisp = 25;
ringBoltR = 2;

toothCutoutBounds = [5, 26, 14+channelZDisp];
toothCutoutDisp = [channelD/2-toothCutoutBounds[0], -toothCutoutBounds[1]/2, ringHeight-toothCutoutBounds[2]];

wireR = 5/2;

wireD = ringID-7;

controllerMount();

module controllerMount() {
    difference() {
        //the bulk of the mount
        intersection() {
            cylinder(ringHeight, r=channelD/2, $fn=fid);
            
            translate([mountXDisp, -mountLength/2, 0])
                cube([100, mountLength, ringHeight]);
            
            //the radially aligned edges
            rotate([0,0,-arcHalfLength]) cube([channelD/2, 200, ringHeight]);
            rotate([0,0,arcHalfLength]) 
                translate([0,-200,0]) cube([channelD/2, 200, ringHeight]);
        }
        
        //cut the bolt points for the mount
        ringBoltAngleDisp = atan2(ringBoltYDisp, channelD/2);
        translate([0,0,ringHeight/2]) {
            rotate([0,90,ringBoltAngleDisp]) cylinder(channelD/2, r=ringBoltR, $fn=fid);
            rotate([0,90,-ringBoltAngleDisp]) cylinder(channelD/2, r=ringBoltR, $fn=fid);
            
        }
        
        //cut out for the tooth bolt
        translate(toothCutoutDisp) cube(toothCutoutBounds); 
        
        //cut out for above  and below the channel
        difference() {
            //cut away an arbitrarily large cylinder
            cylinder(ringHeight, r=200, $fn=fid);
            
            //leave everything inside the ring ID
            cylinder(ringHeight, r=ringID/2, $fn=fid);
            
            //leave everything inside the channel
            translate([0,0,channelZDisp])
                cylinder(ringHeight-channelZDisp*2, r=channelD, $fn=fid);
        }
        
        //cut out where the pcb goes
        translate([mountXDisp,0,ringHeight/2-pcbBounds[2]/2]) {
            translate([-1,-100,0]) cube([pcbBounds[0]+1, 200, pcbBounds[2]]);
            //cut out for the voltage regulators
            translate([pcbBounds[0],11,11]) cube([11, 15, 17.5]);
            //cut out for the xbee
            translate([pcbBounds[0],-13,7]) cube([11, 26, pcbBounds[2]-7]);
            
            //indicator LED holes
            translate([7, 0, -100]) cylinder(200, r=7/2, $fn=fid);
            
        }
        //bolt holes
        translate([mountXDisp,0,ringHeight/2]) {
            translate([0,14,-13.3]) rotate([0,90,0]) 
                cylinder(100, r=3/2, $fn=fid);
            translate([0,-14,-13.3]) rotate([0,90,0]) 
                cylinder(100, r=3/2, $fn=fid);
            translate([0,14,13.3]) rotate([0,90,0]) 
                cylinder(100, r=3/2, $fn=fid);
            translate([0,-14,13.3]) rotate([0,90,0]) 
                cylinder(100, r=3/2, $fn=fid);
        }
        
        //cut a hole for the power pass-through
        
        render() for(angle=[-arcHalfLength:1:arcHalfLength]) {
            rotate([0,0,angle]) translate([wireD/2, -1, 7])
                rotate([-90,0,0]) cylinder(2, r=wireR, $fn=fid);
        }
    }
}